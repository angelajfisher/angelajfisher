<div align="center">
<a href="https://wakatime.com/@018c1e94-8745-411f-aea1-f33be044d952"><img src="https://wakatime.com/badge/user/018c1e94-8745-411f-aea1-f33be044d952.svg" alt="total time coded" /></a> spent coding since Nov 29, 2023 ✨
</div>

# About Me

Hi, I'm Angela, a software engineer specializing in full stack web development! Feel free to take a peek around my repositories here on GitLab, but please note that my most recent work can be found on [GitHub](https://github.com/angelajfisher). While you're here, check out my project [CommuniTEA](https://gitlab.com/angelajfisher/communiTEA), and my custom theme for [Oh My Posh](https://ohmyposh.dev/) below!

---

## CommuniTEA

[CommuniTEA](https://gitlab.com/angelajfisher/communiTEA) is a single-page web application built in React that utilizes a FastAPI with PostgreSQL backend. Its aim is to unite tea-enthusiasts with their local communities around small businesses à la Seattle coffee shop culture.
This app was created in an agile environment alongside two of my classmates as our final project for our web dev certification program. Check out a live demo of the site [here](https://angelajfisher.gitlab.io/communiTEA/)!

![Wiki Screenshot](https://gitlab.com/angelajfisher/angelajfisher/-/raw/main/screenshots/communiTEA/wiki.png)
![Events Screenshot](https://gitlab.com/angelajfisher/angelajfisher/-/raw/main/screenshots/communiTEA/events.png)

---

## Angela's Catppuccin for OMP

[Try my OMP theme](https://gitlab.com/angelajfisher/angelas-catppuccin-for-omp) based on the popular [Catppuccin palette](https://github.com/catppuccin) for some lovely pastel aesthetics alongside tons of added functionality!

![Screenshot](https://gitlab.com/angelajfisher/angelas-catppuccin-for-omp/-/raw/main/Screenshots/Example-1.png)
